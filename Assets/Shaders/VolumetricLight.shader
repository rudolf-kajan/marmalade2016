// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Gamifi.cc/VolumetricLight"
{
	Properties
	{
		_LightColor( "Light color", Color ) = ( 1,1,1,1 )
		_LightTexture( "Light texture", 2D ) = "white" {}
		_SpotStartRadius( "Spot start radius", Float ) = 1.0
		_SpotEndRadius( "Spot end radius", Float ) = 1.0
		_SpotRange( "Spot range", Float ) = 1.0
	}

	SubShader
	{
		Tags
		{
			"RenderType" = "Opaque"
			"LightMode" = "Always"
			"Queue" = "Transparent"
			"ForceNoShadowCasting" = "True"
		}
		LOD 200
		Blend One One
		Lighting Off
		ZWrite Off

		Pass
		{
			CGPROGRAM
			#pragma vertex vertexFunc
			#pragma fragment fragmentFunc
			#include "UnityCG.cginc"

			half4 _LightColor;
			sampler2D _LightTexture;
			half _SpotStartRadius;
			half _SpotEndRadius;
			half _SpotRange;

			struct fragInput
			{
				half4 f_position : SV_POSITION;
				half2 f_texCoord : TEXCOORD0;
				half4 color : COLOR;
			};

			fragInput vertexFunc( appdata_tan v )
			{
				fragInput output;
				half intensity = clamp( distance( _WorldSpaceCameraPos, mul( unity_ObjectToWorld, v.vertex ) ) / 8 - .1, 0, 1 );
				_SpotEndRadius *= intensity;
				_SpotStartRadius *= intensity;
				half endstart = _SpotEndRadius - _SpotStartRadius;
				half4 vertex = 0;
				vertex.xy = v.vertex.xy * ( _SpotStartRadius + v.vertex.z * ( endstart ) );
				vertex.z = v.vertex.z * _SpotRange + _SpotStartRadius;
				vertex.z *= intensity;
				vertex.w = 1.0;

				half3 normal = 0;
				normal.xz = normalize( half2( 1.0, -1 * ( endstart ) / _SpotRange ) );
				normal.xy = normalize( v.vertex.xy ) * ( 1 - normal.z * normal.z );

				half4 vs_position = mul( UNITY_MATRIX_MV, vertex );
				half3 vs_eyeVec = normalize( vs_position.xyz );
				half4 vs_zero = mul( UNITY_MATRIX_MV, half4( 0.0, 0.0, 0.0, 1.0 ) );
				half3 vs_normal = ( mul( UNITY_MATRIX_MV, half4( normal, 1.0 ) ) - vs_zero ).xyz;
				half3 vs_lightVec = ( mul( UNITY_MATRIX_MV, half4( 0.0, 0.0, 1.0, 1.0 ) ) - vs_zero ).xyz;

				half intensity0 = dot( vs_eyeVec, vs_normal );
				intensity0 = intensity0 * intensity0;
				intensity0 = 1.0 - intensity0;
				half intensity1 = dot( vs_eyeVec, vs_lightVec );
				intensity0 = intensity0 * ( 1.0 - pow( abs( intensity1 ), 16.0 ) );

				output.f_position = mul( UNITY_MATRIX_MVP, vertex );
				output.f_texCoord = half2( intensity0 * 0.5 + 0.5, 1.0 - v.vertex.z );

				output.color = _LightColor * 1.85;
				return output;
			}

			half4 fragmentFunc( fragInput i ) : COLOR
			{
				return tex2D( _LightTexture, i.f_texCoord.xy ) * i.color;
			}
			ENDCG
		}

	}
	Fallback "Diffuse"
}

using UnityEngine;
using System.Collections;

public static class Vector3Extensions
{
    public static Vector2 Vec2XY(this Vector3 vec)
    {
        return new Vector2(vec.x, vec.y);
    }

    public static Vector2 Vec2XZ(this Vector3 vec)
    {
        return new Vector2(vec.x, vec.z);
    }

    public static Vector2 Vec2YZ(this Vector3 vec)
    {
        return new Vector2(vec.y, vec.z);
    }

    public static Vector3 SetX(this Vector3 vec, float x)
    {
        return new Vector3(x, vec.y, vec.z);
    }

    public static Vector3 SetY(this Vector3 vec, float y)
    {
        return new Vector3(vec.x, y, vec.z);
    }

    public static Vector3 SetZ(this Vector3 vec, float z)
    {
        return new Vector3(vec.x, vec.y, z);
    }

    public static Vector3 Add(this Vector3 vec, float x, float y, float z)
    {
        return new Vector3(vec.x + x, vec.y + y, vec.z + z);
    }

    public static Vector3 Abs(this Vector3 vec)
    {
        return new Vector3(Mathf.Abs(vec.x), Mathf.Abs(vec.y), Mathf.Abs(vec.z));
    }
}

using UnityEngine;
using System.Collections;

public class Singleton<T> : GameBaseBehaviour where T : GameBaseBehaviour
{
    protected static T instance;

    /// <summary>
    /// Whether instance have already been checked in the scene
    /// </summary>
    protected static bool loaded;

    /// <summary>
    /// Instance object of this singleton
    /// </summary>
    public static T Instance
    {
        get
        {
            if (!loaded)
            {
                // Make sure to find existing object if its Awake method hasn't been called yet
                instance = (T)FindObjectOfType(typeof(T));
                loaded = true;
            }

            return instance;
        }
    }

    public static bool Exists
    {
        get
        {
            return (Instance != null);
        }
    }

    protected sealed override void Awake()
    {
        if (instance != null && instance != this)
        {
            Destroy(gameObject);
            return;
        }

        base.Awake();

        instance = this as T;
        loaded = true;

        InitSingleton();
    }

    /// <summary>
    /// Called only for one singleton instance just after Awake
    /// </summary>
    protected virtual void InitSingleton()
    { }
}

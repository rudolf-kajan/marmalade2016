using UnityEngine;
using System.Collections.Generic;

#if UNITY_EDITOR
    using UnityEditor;
#endif
/// <summary>
/// Base class for all our behaviours!
/// </summary>
public class GameBaseBehaviour : MonoBehaviour
{
    protected Collider myCollider = null;
    public Collider CachedCollider
    {
        get
        {
            if (myCollider == null)
                myCollider = GetComponent<Collider>();
            return myCollider;
        }
    }

    protected Transform myTransform = null;
    public Transform CachedTransform
    {
        get
        {
            if (myTransform == null)
                myTransform = transform;
            return myTransform;
        }
    }

    protected Rigidbody myRigidbody = null;
    public Rigidbody CachedRigidbody
    {
        get
        {
            if (myRigidbody == null)
                myRigidbody = GetComponent<Rigidbody>();
            return myRigidbody;
        }
    }

    protected virtual void Awake()
    {
        //myCollider = collider;
        //myTransform = transform;
        //myRigidbody = rigidbody;
    }

    public delegate void Task();

#if !UNITY_METRO // TODO: Dave pozries sa na to?
    public void Invoke(Task task, float time)
    {
        Invoke(task.Method.Name, time);
    }


    public void InvokeRepeating(Task task, float time, float repeatRate)
    {
        InvokeRepeating(task.Method.Name, time, repeatRate);
    }


    public void CancelInvoke(Task task)
    {
        CancelInvoke(task.Method.Name);
    }
#endif

    public bool IsInLayerMask(GameObject obj, LayerMask mask)
    {
        int objLayerMask = (1 << obj.layer);
        return ((mask & objLayerMask) > 0);
    }

    public Transform FindChildRecursive(string name)
    {
        return FindChildRecursive(CachedTransform, name);
    }

    public List<Transform> FindChildrenRecursive(string name)
    {
        return FindChildrenRecursive(CachedTransform, name);
    }

    public Transform FindChildRecursive(Transform t, string name)
    {
        // Check all children names
        foreach (Transform child in t)
        {
            if (child.name.Equals(name))
            {
                return child;
            }
        }

        // Child not found, run recursive search
        foreach (Transform child in t)
        {
            Transform obj = FindChildRecursive(child, name);
            if (obj != null)
            {
                return obj;
            }
        }

        return null;
    }

    public List<Transform> FindChildrenRecursive(Transform t, string name)
    {
        List<Transform> list = new List<Transform>();

        // Check all children names
        foreach (Transform child in t)
        {
            if (child.name.Equals(name))
            {
                list.Add(child);
            }

            List<Transform> childList = FindChildrenRecursive(child, name);
            list.AddRange(childList);
        }

        return list;
    }
    public I GetInterfaceComponent<I>() where I : class
    {
        return GetComponent(typeof(I)) as I;
    }

    public static GameObject InstantiateWithPrefab(GameObject go)
    {
#if UNITY_EDITOR
        if (Application.isEditor)
        {
            PrefabType pt = PrefabUtility.GetPrefabType(go);
            if (pt == PrefabType.Prefab || pt == PrefabType.ModelPrefab)
            {
                return (GameObject)PrefabUtility.InstantiatePrefab(go);
            }
        }
#endif

        return (GameObject)Instantiate(go);
    }

    public static T InstantiateWithPrefab<T>(T obj) where T : MonoBehaviour
    {
        GameObject go = InstantiateWithPrefab(obj.gameObject);

        return go.GetComponent<T>();
    }
}

using UnityEngine;
using System.Collections;

public static class Vector2Extensions
{
    public static Vector2 Abs(this Vector2 vec)
    {
        return new Vector2(Mathf.Abs(vec.x), Mathf.Abs(vec.y));
    }

    public static Vector3 Vec3XY(this Vector2 vec)
    {
        return new Vector3(vec.x, vec.y, 0);
    }

    public static Vector3 Vec3XY(this Vector2 vec, float z)
    {
        return new Vector3(vec.x, vec.y, z);
    }

    public static Vector3 Vec3XZ(this Vector2 vec)
    {
        return new Vector3(vec.x, 0, vec.y);
    }

    /// <summary>
    /// Flips the Y coordinate according to Screen.height
    /// </summary>
    /// <param name="vec"></param>
    /// <returns></returns>
    public static Vector2 FlipY(this Vector2 vec)
    {
        return new Vector2(vec.x, Screen.height - vec.y);
    }

    public static Vector2 NegateX(this Vector2 vec)
    {
        return new Vector2(-vec.x, vec.y);
    }

    public static Vector2 NegateY(this Vector2 vec)
    {
        return new Vector2(vec.x, -vec.y);
    }
}

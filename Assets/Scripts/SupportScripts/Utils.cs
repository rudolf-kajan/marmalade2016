using System.Text;
using UnityEngine;
using System.Collections;

public static class Utils
{
    public static bool IsInRange(float val, float a, float b)
    {
        if (a > b)
        {
            return (val >= b && val <= a);
        }
        else
        {
            return (val >= a && val <= b);
        }
    }

    /// <summary>
    ///     Clears the contents of the string builder.
    /// </summary>
    /// <param name="value">
    ///     The <see cref="StringBuilder"/> to clear.
    /// </param>
    public static void Clear(this StringBuilder value)
    {
        value.Length = 0;
        value.Capacity = 0;
    }
}

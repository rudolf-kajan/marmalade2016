using UnityEngine;
using System.Collections;

public abstract class BaseEvent
{
    public GameBaseBehaviour Sender { get; private set; }

    public BaseEvent(GameBaseBehaviour sender)
    {
        Sender = sender;
    }
}

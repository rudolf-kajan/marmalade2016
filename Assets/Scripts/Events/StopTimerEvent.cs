﻿public class StopTimerEvent : BaseEvent
{
    public StopTimerEvent(GameBaseBehaviour sender)
        : base(sender)
    { }
}
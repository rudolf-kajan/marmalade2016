using UnityEngine;
using System.Collections;

public delegate void EventHandler<T>(T data) where T : BaseEvent;

public class EventManager : Singleton<EventManager>
{
    static class PerType<T> where T : BaseEvent
    {
        public static EventHandler<T> delegates;
    }

    public static void Register<T>(EventHandler<T> handler) where T : BaseEvent
    {
        if (Instance != null)
            Instance.InternalRegister(handler);
    }

    public static void Unregister<T>(EventHandler<T> handler) where T : BaseEvent
    {
        if (Instance != null)
            Instance.InternalUnregister(handler);
    }

    public static void FireEvent<T>(T data) where T : BaseEvent
    {
        if (Instance != null)
            Instance.InternalFireEvent(data);
    }

    protected void InternalRegister<T>(EventHandler<T> handler) where T : BaseEvent
    {
        PerType<T>.delegates += handler;
    }

    protected void InternalUnregister<T>(EventHandler<T> handler) where T : BaseEvent
    {
        PerType<T>.delegates -= handler;
    }

    protected void InternalFireEvent<T>(T data) where T : BaseEvent
    {
        if (PerType<T>.delegates != null)
            PerType<T>.delegates(data);
    }
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class TimeManager : MonoBehaviour {

    public Text TimeText;
    private TimeSpan time;

    private bool stopped;

	// Use this for initialization
	void Start () {
        time = TimeSpan.FromMilliseconds(0);
        EventManager.Register<StopTimerEvent>(OnStopTimer);
	}

    private void OnStopTimer(StopTimerEvent data)
    {
        stopped = true;
    }
	
	// Update is called once per frame
	void Update () {
        if (!stopped)
        {
            time += TimeSpan.FromSeconds(Time.deltaTime);
            TimeText.text = (time.TotalMilliseconds / 1000.0f).ToString("F");
        }
	}
}

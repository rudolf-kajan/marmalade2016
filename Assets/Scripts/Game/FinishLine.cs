﻿using UnityEngine;
using System.Collections;

public class FinishLine : GameBaseBehaviour
{
    void OnTriggerEnter(Collider collider)
    {
        CarController buggy = collider.GetComponentInParent<CarController>();
        if (buggy != null)
        {
            EventManager.FireEvent<StopTimerEvent>(new StopTimerEvent(this));
        }
    }
}

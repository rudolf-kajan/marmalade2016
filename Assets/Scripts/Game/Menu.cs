﻿using UnityEngine;
using System.Collections;
using InControl;

public class Menu : MonoBehaviour
{
    public GameObject MenuCamera;

    private bool paused = false;

    void Start()
    {
        Pause();
    }

    private void Pause()
    {
        paused = true;
        MenuCamera.SetActive(true);
        Time.timeScale = 0;
    }

    private void Resume()
    {
        paused = false;
        MenuCamera.SetActive(false);
        Time.timeScale = 1;
    }

    void Update()
    {
        if (InputManager.ActiveDevice.Action1.IsPressed &&
            Input.GetKey(KeyCode.Return) &&
            Input.GetMouseButton(0) &&
            paused)
        {
            if (InputManager.ActiveDevice.Action1.WasPressed ||
                Input.GetKeyDown(KeyCode.Return) ||
                Input.GetMouseButtonDown(0))
            {
                Resume();
            }
        }

        if (InputManager.ActiveDevice.Action3.IsPressed &&
            Input.GetKey(KeyCode.Escape) &&
            Input.GetMouseButton(1))
        {
            if (InputManager.ActiveDevice.Action3.WasPressed ||
                Input.GetKeyDown(KeyCode.Escape) ||
                Input.GetMouseButtonDown(1))
            {
                if (paused)
                {
                    Application.Quit();
                }
                else
                {
                    Pause();
                }
            }
        }
    }
}

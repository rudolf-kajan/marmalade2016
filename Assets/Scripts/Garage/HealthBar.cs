﻿using UnityEngine;
using System.Collections;

public class HealthBar : MonoBehaviour
{
    public MeshRenderer Quad;

    private float percentage;
    public float Percentage
    {
        get { return percentage; }
        set
        {
            transform.localScale = transform.localScale.SetX(value);
            percentage = value;
        }
    }

    void Awake()
    {
        Percentage = 1.0f;
    }
}

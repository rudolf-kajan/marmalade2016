﻿using UnityEngine;
using System.Collections;

public class TintIcon : MonoBehaviour
{
    public Color Color1;
    public Color Color2;

    private Material mat;

    private float percentage;
    public float Percentage
    {
        get { return percentage; }
        set
        {
            percentage = value;
            mat.color = Color.Lerp(Color2, Color1, percentage);
        }
    }

    void Awake()
    {
        mat = GetComponent<MeshRenderer>().material;
        Percentage = 1;
    }
}

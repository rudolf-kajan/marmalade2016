﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum ItemType
{
    Fuel,
    Nitro,
    RepairWheels,
    RepairEngine,
}

public class Inventory : MonoBehaviour
{
    public CarController Buggy;
    public Camera PickCamera;

    public List<MeshRenderer> Slots;
    public Material EmptyIcon;
    public Material FuelIcon;
    public Material NitroIcon;
    public Material RepairWheelsIcon;
    public Material RepairEngineIcon;

    public Collider FLWheelSlot;
    public Collider FRWheelSlot;
    public Collider RLWheelSlot;
    public Collider RRWheelSlot;
    public Collider EngineSlot;
    public Collider FuelSlot;
    public Collider NitroSlot;

    public AudioSource WheelRepairedAudio;
    public AudioSource AddedNitroAudio;
    public AudioSource AddedGasAudio;

    private List<ItemType> items;

    private bool dragging;
    private int draggedSlot;
    private Transform draggedObj;
    private Vector3 dragOrigPos;
    

    void Awake()
    {
        items = new List<ItemType>();
    }

    void Start()
    {
        EventManager.Register<FuelAddedEvent>(OnFuelAdded);
        EventManager.Register<NitroAddedEvent>(OnNitroAdded);
        EventManager.Register<WheelRepairEvent>(OnWheelRepairAdded);
        EventManager.Register<EngineRepairEvent>(OnEngineRepairAdded);
    }

    private void OnEngineRepairAdded(EngineRepairEvent data)
    {
        OnItemAdded(ItemType.RepairEngine);
    }

    private void OnWheelRepairAdded(WheelRepairEvent data)
    {
        OnItemAdded(ItemType.RepairWheels);
    }

    private void OnNitroAdded(NitroAddedEvent data)
    {
        OnItemAdded(ItemType.Nitro);
    }

    private void OnFuelAdded(FuelAddedEvent data)
    {
        OnItemAdded(ItemType.Fuel);
    }

    private void OnItemAdded(ItemType item)
    {
        if (items.Count >= Slots.Count)
        {
            // Ignore
            return;
        }

        // Add to list
        items.Add(item);

        RedrawSlots();
    }

    private void RedrawSlots()
    {
        for (int i = 0; i < Slots.Count; i++)
        {
            if (i >= items.Count)
            {
                Slots[i].material = EmptyIcon;
                continue;
            }

            switch (items[i])
            {
                case ItemType.Fuel:
                    Slots[i].material = FuelIcon;
                    break;
                case ItemType.Nitro:
                    Slots[i].material = NitroIcon;
                    break;
                case ItemType.RepairWheels:
                    Slots[i].material = RepairWheelsIcon;
                    break;
                case ItemType.RepairEngine:
                    Slots[i].material = RepairEngineIcon;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }

    void Update()
    {
        HandleDragging();
    }

    private void HandleDragging()
    {
        if (!dragging)
        {
            if (!Input.GetMouseButtonDown(0))
                return;

            RaycastHit hit;
            if (!Physics.Raycast(PickCamera.ScreenPointToRay(Input.mousePosition), out hit))
                return;

            // Find icon
            MeshRenderer rnd = hit.collider.GetComponent<MeshRenderer>();
            if (rnd == null)
                return;

            for (int i = 0; i < Slots.Count; i++)
            {
                if (Slots[i] == rnd)
                {
                    // Check if there's some item in the slot
                    if (i < items.Count)
                    {
                        StartDragging(i);
                    }
                    return;
                }
            }
        }
        else
        {
            if (Input.GetMouseButtonUp(0))
            {
                StopDragging();
                return;
            }

            Vector3 screenPos = Input.mousePosition.SetZ(dragOrigPos.z);
            Vector3 worldPos = PickCamera.ScreenToWorldPoint(screenPos);
            draggedObj.position = worldPos;
        }
    }

    private void StartDragging(int slot)
    {
        dragging = true;
        draggedSlot = slot;
        draggedObj = Slots[slot].transform;
        dragOrigPos = draggedObj.position;
    }

    private void StopDragging()
    {
        // Check if we're dropping on car slot
        int carSlotLayerMask = 1 << 11;
        RaycastHit hit;
        if (Physics.Raycast(PickCamera.ScreenPointToRay(Input.mousePosition), out hit, float.PositiveInfinity, carSlotLayerMask))
        {
            if (hit.collider == FLWheelSlot)
            {
                if (items[draggedSlot] == ItemType.RepairWheels)
                {
                    Buggy.RepairFLWheel();
                    if (WheelRepairedAudio != null)
                        WheelRepairedAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == FRWheelSlot)
            {
                if (items[draggedSlot] == ItemType.RepairWheels)
                {
                    Buggy.RepairFRWheel();
                    if (WheelRepairedAudio != null)
                        WheelRepairedAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == RLWheelSlot)
            {
                if (items[draggedSlot] == ItemType.RepairWheels)
                {
                    Buggy.RepairRLWheel();
                    if (WheelRepairedAudio != null)
                        WheelRepairedAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == RRWheelSlot)
            {
                if (items[draggedSlot] == ItemType.RepairWheels)
                {
                    Buggy.RepairRRWheel();
                    if (WheelRepairedAudio != null)
                        WheelRepairedAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == EngineSlot)
            {
                if (items[draggedSlot] == ItemType.RepairEngine)
                {
                    Buggy.RepairEngine();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == FuelSlot)
            {
                if (items[draggedSlot] == ItemType.Fuel)
                {
                    Buggy.AddGas();
                    if (AddedGasAudio != null)
                        AddedGasAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
            else if (hit.collider == NitroSlot)
            {
                if (items[draggedSlot] == ItemType.Nitro)
                {
                    Buggy.StartNitro();
                    if (AddedNitroAudio != null)
                        AddedNitroAudio.Play();
                }
                ClearSlot(draggedSlot);
            }
        }

        draggedObj.position = dragOrigPos;
        dragging = false;
        draggedObj = null;
    }

    private void ClearSlot(int slot)
    {
        items.RemoveAt(slot);
        RedrawSlots();
    }
}

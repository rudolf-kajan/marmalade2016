﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using InControl;

[System.Serializable]
public class AxleInfo
{
    public WheelCollider LeftWheel;
    public WheelCollider RightWheel;
    public bool Motor; // is this wheel attached to motor?
    public bool Steering; // does this wheel apply steer angle?
    public bool HandBrake;
    public float TorqueRatio = 0.5f;

    public Transform LeftWheelVisual;
    public Transform RightWheelVisual;

    public TintIcon LeftWheelHealthBar;
    public TintIcon RightWheelHealthBar;

    public float LeftWheelDurability = 100;
    public float RightWheelDurability = 100;

    public ParticleSystem LeftTyreDirt;
    public ParticleSystem RightTyreDirt;
    public float TyreDirtRatio = 0.3f;

    public void Update()
    {
        ApplyVisualPosition(LeftWheel, LeftWheelVisual);
        ApplyVisualPosition(RightWheel, RightWheelVisual);

        LeftWheelDurability -= GetWheelDamage(LeftWheel);
        RightWheelDurability -= GetWheelDamage(RightWheel);
        LeftWheelDurability = Mathf.Max(LeftWheelDurability, 0);
        RightWheelDurability = Mathf.Max(RightWheelDurability, 0);

        if (LeftWheelDurability <= 0)
        {
            LeftWheelVisual.gameObject.SetActive(false);
            LeftWheel.enabled = false;
            LeftTyreDirt.gameObject.SetActive(false);
        }
        if (RightWheelDurability <= 0)
        {
            RightWheelVisual.gameObject.SetActive(false);
            RightWheel.enabled = false;
            RightTyreDirt.gameObject.SetActive(false);
        }

        // Tyre dirt
        if (LeftTyreDirt != null)
        {
            float rate = Mathf.Min(Mathf.Abs(LeftWheel.rpm), 2000) * TyreDirtRatio;
            var em = LeftTyreDirt.emission;
            em.rate = new ParticleSystem.MinMaxCurve(rate);
        }
        if (RightTyreDirt != null)
        {
            float rate = Mathf.Min(Mathf.Abs(RightWheel.rpm), 2000) * TyreDirtRatio;
            var em = RightTyreDirt.emission;
            em.rate = new ParticleSystem.MinMaxCurve(rate);
        }

        UpdateHealthBars();
    }

    private void UpdateHealthBars()
    {
        LeftWheelHealthBar.Percentage = LeftWheelDurability / 100.0f;
        RightWheelHealthBar.Percentage = RightWheelDurability / 100.0f;
    }

    public void RepairWheels(float left, float right)
    {
        if (LeftWheelDurability > 0)
        {
            LeftWheelDurability += left;
            LeftWheelDurability = Mathf.Min(LeftWheelDurability, 100);
        }
        if (RightWheelDurability > 0)
        {
            RightWheelDurability += right;
            RightWheelDurability = Mathf.Min(RightWheelDurability, 100);
        }

        UpdateHealthBars();
    }

    private void ApplyVisualPosition(WheelCollider wheel, Transform visual)
    {
        if (visual == null)
            return;

        Vector3 position;
        Quaternion rotation;
        wheel.GetWorldPose(out position, out rotation);

        visual.position = position;
        visual.rotation = rotation;
    }

    private float GetWheelDamage(WheelCollider wheel)
    {
        float damage = 0;

        WheelHit hit;
        if (wheel.GetGroundHit(out hit))
        {
            float fwdSlip = Mathf.Max(Mathf.Abs(hit.forwardSlip) - 0.4f);
            if (fwdSlip > 0.4f)
            {
                damage += fwdSlip * 5;
            }

            float sideSlip = Mathf.Max(Mathf.Abs(hit.sidewaysSlip) - 0.4f);
            if (sideSlip > 0.4f)
            {
                damage += sideSlip * 5;
            }

            float dmgForce = Mathf.Max(Mathf.Abs(hit.force) - 7000, 0);
            damage += dmgForce / 500.0f;

            damage = damage * Time.fixedDeltaTime;

        }

        return damage;
    }
}

public class CarController : MonoBehaviour
{
    const float WheelRepairValue = 6;
    const float EngineRepairValue = 6;
    const float FuelFillValue = 10;
    const float GasMax = 50;

    public float NitroTime = 3.0f;
    public float NitroPower = 100.0f;

    public List<AxleInfo> Axles;
    public float EngineTorque;
    public float BrakeTorque;
    public float MaxSteering;

    public TintIcon EngineHealthBar;
    public TintIcon FuelHealthBar;

    private bool reverse = false;

    private float wheelsSpeed = 0;
    private float wheelsRpm = 0;

    private int drivenAxles = 1;

    public float EngineDurability = 100;

    public float Gas = GasMax;

    public AnimationCurve TorqueCurve;
    public float DriveTrainRatio = 4.0f;

    public Light LeftLight;
    public Light RightLight;

    public float LightIntensity = 8.0f;
    public float LightFadeSpeed = 1.0f;
    private float lightTime;

    public List<ParticleSystem> Exhausts;

    public Rigidbody mainRigidbody;
    private float nitroActive = 0.0f;

    public bool DebugGui = false;

    public List<Transform> Checkpoints;

    void Awake()
    {
        mainRigidbody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        drivenAxles = 0;
        for (int i = 0; i < Axles.Count; i++)
        {
            if (Axles[i].Motor)
            {
                drivenAxles++;
            }
        }
    }

    private float GetWheelVelocity(WheelCollider wheel)
    {
        return 2.0f * Mathf.PI * wheel.radius * wheel.rpm * 60.0f / 1000.0f;
    }

    private void UpdateWheelsInfo()
    {
        float speedSum = 0;
        float rpmSum = 0;
        for (int i = 0; i < Axles.Count; i++)
        {
            speedSum += GetWheelVelocity(Axles[i].LeftWheel);
            speedSum += GetWheelVelocity(Axles[i].RightWheel);
            rpmSum += Axles[i].LeftWheel.rpm;
            rpmSum += Axles[i].RightWheel.rpm;
        }
        wheelsSpeed = Mathf.Round(speedSum / (Axles.Count * 2));
        wheelsRpm = rpmSum / (Axles.Count * 2);
    }

    public void UpdateEngineDurability()
    {
        float dmg = Mathf.Min(wheelsSpeed, 100) / 60.0f * Time.fixedDeltaTime;
        EngineDurability -= dmg;
        EngineHealthBar.Percentage = EngineDurability / 100.0f;
    }

    void OnGUI()
    {
        if (!DebugGui)
            return;

        GUI.color = Color.black;
        GUI.Label(new Rect(0, 0, 400, 30), "Speed: " + wheelsSpeed.ToString() + " km/h");
        GUI.Label(new Rect(0, 30, 400, 30), "RPM: " + Mathf.Round(wheelsRpm * DriveTrainRatio).ToString());
        GUI.Label(new Rect(0, 60, 400, 30), "Engine: " + Mathf.Round(EngineDurability).ToString() + "%");
        GUI.Label(new Rect(0, 90, 400, 30), "Gas: " + Gas.ToString());
        float top = 120;
        for (int i = 0; i < Axles.Count; i++)
        {
            string lw = Mathf.Round(Axles[i].LeftWheelDurability).ToString();
            WheelHit hit;
            if (Axles[i].LeftWheel.GetGroundHit(out hit))
            {
                lw += ", Force: " + Mathf.Round(hit.force).ToString() + ", Slip: " + hit.forwardSlip.ToString();
            }
            GUI.Label(new Rect(0, top, 400, 30), lw);
            top += 30;
            string rw = Mathf.Round(Axles[i].RightWheelDurability).ToString();
            if (Axles[i].RightWheel.GetGroundHit(out hit))
            {
                rw += ", Force: " + Mathf.Round(hit.force).ToString() + ", Slip: " + hit.forwardSlip.ToString();
            }
            GUI.Label(new Rect(0, top, 400, 30), rw);
            top += 30;
        }
    }

    private void ResetCar()
    {
        if (Checkpoints.Count == 0)
            return;

        // Reset car - find the closest checkpoint
        int closest = 0;
        float minDist = float.PositiveInfinity;
        Vector3 pos = transform.position;
        for (int i = 0; i < Checkpoints.Count; i++)
        {
            float dist = Vector3.Distance(pos, Checkpoints[i].position);
            if (dist < minDist)
            {
                minDist = dist;
                closest = i;
            }
        }

        // Move car one checkpoint back
        if (closest > 0)
        {
            closest--;
        }

        transform.position = Checkpoints[closest].position;
        transform.rotation = Checkpoints[closest].rotation;
        mainRigidbody.velocity = Vector3.zero;
        mainRigidbody.angularVelocity = Vector3.zero;
    }

    // Use this for initialization
    // Update is called once per frame
    void FixedUpdate()
    {
        // Handle car reset
        if (InputManager.ActiveDevice.Action4.WasPressed)
        {
            ResetCar();
        }

        if (LeftLight != null && RightLight != null)
        {
            lightTime = Input.GetKeyDown(KeyCode.Space) ? 0.0f : lightTime + Time.fixedDeltaTime * LightFadeSpeed;
            float intensity = Mathf.Max(0.0f, (LightIntensity / lightTime) - 1.0f);
            LeftLight.intensity = intensity;
            RightLight.intensity = intensity;
        }

        // Determine controls according to reversing
        float torque = 0, brake = 0;
        if (!reverse)
        {
            torque = EngineTorque * InputManager.ActiveDevice.RightTrigger;
            brake = InputManager.ActiveDevice.LeftTrigger.IsPressed ? BrakeTorque : 0;
        }
        else
        {
            torque = -EngineTorque * InputManager.ActiveDevice.LeftTrigger;
            brake = InputManager.ActiveDevice.RightTrigger.IsPressed ? BrakeTorque : 0;
        }

        torque = torque * TorqueCurve.Evaluate(Mathf.Abs(wheelsRpm) * DriveTrainRatio);
        float steer = MaxSteering * InputManager.ActiveDevice.Direction.X;

        // Engine damage
        torque *= 0.5f + (EngineDurability / 100.0f * 0.5f);

        // Check gas
        if (Gas <= 0)
        {
            torque = 0;
        }

        // Update exhausts
        float rate = Mathf.Min(60 + Mathf.Abs(wheelsRpm) * 0.3f, 500);
        for (int i = 0; i < Exhausts.Count; i++)
        {
            var em = Exhausts[i].emission;
            em.rate = new ParticleSystem.MinMaxCurve(rate);
        }

        if (InputManager.ActiveDevice.RightBumper.WasPressed)
        {
            // Make bigger wheels
            for (int i = 0; i < Axles.Count; i++)
            {
                Axles[i].LeftWheel.radius *= 1.2f;
                Axles[i].RightWheel.radius *= 1.2f;
                Axles[i].LeftWheel.forceAppPointDistance *= 1.2f;
                Axles[i].RightWheel.forceAppPointDistance *= 1.2f;
                Axles[i].LeftWheelVisual.localScale *= 1.2f;
                Axles[i].RightWheelVisual.localScale *= 1.2f;
            }
            EngineTorque *= 1.2f;
        }
        if (InputManager.ActiveDevice.LeftBumper.WasPressed)
        {
            // Make bigger wheels
            for (int i = 0; i < Axles.Count; i++)
            {
                Axles[i].LeftWheel.radius /= 1.2f;
                Axles[i].RightWheel.radius /= 1.2f;
                Axles[i].LeftWheel.forceAppPointDistance /= 1.2f;
                Axles[i].RightWheel.forceAppPointDistance /= 1.2f;
                Axles[i].LeftWheelVisual.localScale /= 1.2f;
                Axles[i].RightWheelVisual.localScale /= 1.2f;
            }
            EngineTorque /= 1.2f;
        }

        // Apply torque and breaks
        for (int i = 0; i < Axles.Count; i++)
        {
            float torqueRatio = Axles[i].TorqueRatio * 0.5f;

            if (Axles[i].Steering)
            {
                Axles[i].LeftWheel.steerAngle = steer;
                Axles[i].RightWheel.steerAngle = steer;
            }
            if (Axles[i].Motor)
            {
                Axles[i].LeftWheel.motorTorque = torque * torqueRatio;
                Axles[i].RightWheel.motorTorque = torque * torqueRatio;
                Axles[i].LeftWheel.brakeTorque = brake * torqueRatio;
                Axles[i].RightWheel.brakeTorque = brake * torqueRatio;
            }

            if (Axles[i].HandBrake && InputManager.ActiveDevice.Action3.IsPressed)
            {
                Axles[i].LeftWheel.brakeTorque = 2000;
                Axles[i].RightWheel.brakeTorque = 2000;
            }
        }

        // Apply nitro - but only if we have some gas
        if (nitroActive > 0)
        {
            if (Gas > 0)
            {
                mainRigidbody.AddForce(mainRigidbody.transform.forward * NitroPower, ForceMode.Acceleration);
            }
            nitroActive -= Time.fixedDeltaTime;
        }

        // Update speed
        UpdateWheelsInfo();

        // Update wheels
        UpdateEngineDurability();
        for (int i = 0; i < Axles.Count; i++)
        {
            Axles[i].Update();
        }

        // Gas consumption
        Gas -= Mathf.Clamp(Mathf.Abs(wheelsRpm), 50, 3000) * Time.fixedDeltaTime * 0.003f;
        Gas = Mathf.Max(Gas, 0);
        FuelHealthBar.Percentage = Gas / GasMax;

        // Handle reverse
        if (wheelsSpeed == 0)
        {
            if (!reverse)
            {
                if (!InputManager.ActiveDevice.RightTrigger.IsPressed && InputManager.ActiveDevice.LeftTrigger.IsPressed)
                {
                    reverse = true;
                }
            }
            else
            {
                if (InputManager.ActiveDevice.RightTrigger.IsPressed && !InputManager.ActiveDevice.LeftTrigger.IsPressed)
                {
                    reverse = false;
                }
            }
        }
	}

    public void RepairFLWheel()
    {
        Axles[0].RepairWheels(WheelRepairValue, 0);
    }

    public void RepairFRWheel()
    {
        Axles[0].RepairWheels(0, WheelRepairValue);
    }

    public void RepairRLWheel()
    {
        Axles[1].RepairWheels(WheelRepairValue, 0);
    }

    public void RepairRRWheel()
    {
        Axles[1].RepairWheels(0, WheelRepairValue);
    }

    public void RepairEngine()
    {
        EngineDurability += EngineRepairValue;
        EngineDurability = Mathf.Min(EngineDurability, 100);
    }

    public void AddGas()
    {
        Gas += FuelFillValue;
        Gas = Mathf.Min(Gas, GasMax);
    }

    public void StartNitro()
    {
        nitroActive = NitroTime;
    }
}

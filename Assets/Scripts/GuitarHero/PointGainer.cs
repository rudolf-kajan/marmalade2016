﻿using UnityEngine;
using System.Collections;
using InControl;
using System.Collections.Generic;
using UnityEngine.UI;

public class PointGainer : GameBaseBehaviour {

    public AudioSource Success;
    public AudioSource Fail;

    public Text DebugText;

    private Point currentPoint;

    private int streakLength;
    private PointType streakType;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyUp(KeyCode.Space))
        {
            if (currentPoint == null && this.streakLength > 0)
                EndStreak();

            if (currentPoint == null)
                return;

            if (this.streakLength == 0)
            {
                BuildUpStreak();
                this.streakType = this.currentPoint.Type;
                return;
            }

            if(this.streakLength > 0)
            {
                if (this.currentPoint.Type != this.streakType)
                {
                    EndStreak();
                }
                else
                {
                    BuildUpStreak();
                }
            }
            
            
            //// debug stuff    
            //if(this.streakLength == 1)
            //{
            //    DebugText.text = "";
            //}

            //if (this.streakLength > 1)
            //{
            //    DebugText.text = "STREAK " + this.streakLength + " x " + this.streakType;
            //}

        }
	}

    void OnTriggerEnter(Collider other)
    {
        currentPoint = other.transform.parent.gameObject.GetComponent<Point>();
    }

    void OnTriggerExit(Collider other)
    {
        if (this.currentPoint != null && !this.currentPoint.WasHit && this.streakLength > 0 && this.currentPoint.Type == this.streakType)
        {
            EndStreak();
        }
            
        currentPoint = null;
    }

    private void BuildUpStreak()
    {
        if (Success != null)
            Success.Play();

        this.currentPoint.SetHitMaterial();
        this.streakLength += 1;

        if(streakLength == 1)
            DebugText.text = "";

        if (streakType == PointType.Fuel && this.streakLength == 2)
        {
            EventManager.FireEvent<FuelAddedEvent>(new FuelAddedEvent(this));
            EndStreak();
            DebugText.text = "FUEL!!!";
        }

        if (streakType == PointType.Nitro && this.streakLength == 4)
        {
            EventManager.FireEvent<NitroAddedEvent>(new NitroAddedEvent(this));
            EndStreak();
            DebugText.text = "NITROOOOOOO!!!";
        }

        if (streakType == PointType.EngineRepair && this.streakLength == 3)
        {
            EventManager.FireEvent<EngineRepairEvent>(new EngineRepairEvent(this));
            EndStreak();
            DebugText.text = "ENGINE REPAIR 5%";
        }

        if (streakType == PointType.WheelRepair && this.streakLength == 2)
        {
            EventManager.FireEvent<WheelRepairEvent>(new WheelRepairEvent(this));
            EndStreak();
            DebugText.text = "WHEEL REPAIR 3%";
        }
    }

    private void EndStreak()
    {
        if (Fail != null)
            Fail.Play();

        if (this.currentPoint != null)
            this.currentPoint.SetMissMaterial();
        this.streakLength = 0;
        this.DebugText.text = "STREAK ENDED";
    }
}

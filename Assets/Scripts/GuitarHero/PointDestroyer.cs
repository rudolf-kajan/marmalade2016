﻿using UnityEngine;
using System.Collections;

public class PointDestroyer : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
       Point point = other.transform.parent.gameObject.GetComponent<Point>();
       point.IsActive = false;

    }
}

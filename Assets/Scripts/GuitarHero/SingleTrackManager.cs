﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SingleTrackManager : MonoBehaviour {

   
    public GameObject Spawner;
    public GameObject[] Spawnables;

    public double MinDelay = 0.5f;
    public double MaxDelay = 2.0f;

    public double Speed = 10f;
    public double SpeedAffect = 0.25;

    public GameObject Buggy;

    private double delay = 0;

    private List<Point> points = new List<Point>();

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        double speedAffector = 0.0;
        if (Buggy != null)
        {
            speedAffector = Buggy.GetComponent<Rigidbody>().velocity.magnitude * SpeedAffect;
        }

        if(delay > 0)
        {
            delay -= Time.deltaTime;
        }
        else
        {
            GameObject point = (GameObject)GameObject.Instantiate(this.Spawnables[Random.Range(0,this.Spawnables.Length)], this.transform);
            point.transform.transform.rotation = this.Spawner.transform.rotation;
            point.transform.transform.position = this.Spawner.transform.position;
            Point p = point.GetComponent<Point>();
            p.IsActive = true;
            points.Add(p);
            delay = MinDelay + UnityEngine.Random.value * (MaxDelay-MinDelay);
        }

        for (int i = points.Count-1; i >= 0; i--)
        {
            Point point = points[i];

            if(point.IsActive)
            {
                double speed = Speed + speedAffector;
                point.transform.localPosition = new Vector3(point.transform.localPosition.x - (float)(Time.deltaTime * speed), point.transform.localPosition.y, point.transform.localPosition.z);
            }
            else
            {
                points.Remove(point);
                Destroy(point.gameObject);
            }

        }
        
	}
}

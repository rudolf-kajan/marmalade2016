﻿using UnityEngine;
using System.Collections;
using System;

public enum PointType
{
    Fuel,
    Nitro,
    WheelRepair,
    EngineRepair
}

public class Point : MonoBehaviour {

    public Material HitMaterial;
    public Material MissMaterial;
    public PointType Type;

    public bool IsActive;

    public bool WasHit {
        get; private set; 
    } 

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    internal void SetMissMaterial()
    {
        if(!WasHit)
            this.gameObject.GetComponentInChildren<Renderer>().material = this.MissMaterial;
    }

    internal void SetHitMaterial()
    {
        this.WasHit = true;
        this.gameObject.GetComponentInChildren<Renderer>().material = this.HitMaterial;
    }
}

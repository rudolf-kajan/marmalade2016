﻿using UnityEngine;
using System.Collections;

public class MusicManager : Singleton<MusicManager> {

    public AudioSource ForestAmbient;
    public AudioSource[] Tracks;
    private AudioSource source;

	// Use this for initialization
	void Start () {

        source = Tracks[Random.Range(0, Tracks.Length)];
        source.Play();

        ForestAmbient.Play();
    }
	
	// Update is called once per frame
	void Update () {

        if (!source.isPlaying)
        {
            source = Tracks[Random.Range(0, Tracks.Length)];
            source.Play();
        }
	
	}
}

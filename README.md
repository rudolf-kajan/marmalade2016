# README #

This README describes who-what-where-how.

### How do I know what is in the game and how it should work? ###

* Read GDD wiki document here: https://bitbucket.org/rudolf-kajan/marmalade2016/wiki/GDD

### How do I know what are my tasks? ###

* See overview of assigned tasks here: https://bitbucket.org/rudolf-kajan/marmalade2016/wiki/Home